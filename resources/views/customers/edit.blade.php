@extends('layouts.app')

@section('content')

<form method="post"  action="{{action('CustomerController@update', $customer->id) }}">
 @method('PATCH')
 @csrf
 <div class="form-group">
   <label for="title">Update name</label>
   <input type="text" class="form-control" name="name" value="{{ $customer->name}}" />
   <label for="title">Update email</label>
   <input type="text" class="form-control" name="email" value="{{ $customer->email}}" />
   <label for="title">Update phone</label>
   <input type="text" class="form-control" name="phone" value="{{ $customer->phone}}" />
 </div>
 <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection
