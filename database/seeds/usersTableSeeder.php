<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class usersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
              [
               'name' => 'moshe',
               'role' => 'manager',
               'email' => 'a@a.com',
               'password'=>Hash::make('12345678'),
               'created_at' => date('Y-m-d G:i:s'),
              ],
                  [ 
                   'name' => 'Harry Poter2',
                   'role' => 'salesrep',
                   'email' => 'b@b.com',
                   'password' =>Hash::make ('12345678'),
                   'created_at' => date('Y-m-d G:i:s'),
                  ],
                  [ 
                    'name' => 'Harry Poter3',
                    'role' => 'salesrep',
                    'email' => 'c@c.com',
                    'password' =>Hash::make ('12345678'),
                    'created_at' => date('Y-m-d G:i:s'),
                   ],
                   
            ]);   
    }        
}
