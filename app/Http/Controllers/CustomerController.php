<?php

namespace App\Http\Controllers;

use App\Customer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function done($id)
    {
        //only if this todo belongs to user         
        $customer = Customer::findOrFail($id);
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");}            
        $customer->status = 1; 
        $customer->save();
        return redirect('customers');    
    }    

    
     public function index()
    {
        if (Gate::denies('manager')){  
        if (Gate::denies('salesrep')) {
            abort(403,"Are you a hacker or what?");} }  
        $customer= Customer::all();
        return view('customers.index',['customers' => $customer]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer= new Customer();
        $id =Auth::id();//the idea of the current user
        $boss = DB::table('users')->where('id',$id)->first();
        $username = $boss->name;
        $customer->username = $username;
        $customer->user_id = $id;
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->status = 0;
        $customer->save();
        return redirect('customers');
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        if (Gate::denies('manager')) {
            if ($customer->user_id!=$id) {
                abort(403,"Are you a hacker or what?");} }   
        return view('customers.edit', compact('customer'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $customer->update($request -> all());
        return redirect('customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findorfail($id);
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");}
        $customer->delete();
        return redirect('customers');
    }
}
